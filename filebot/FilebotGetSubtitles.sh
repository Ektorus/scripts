#!/bin/bash

################################################################################
### Global variables
################################################################################
SLEEP_DELAY=2

################################################################################
### The script
################################################################################
function download_subtitles {
	cur_dir="$1"

	for file in "$cur_dir"/*; do
		if [ -d "$file" ]; then
			echo "Searching for missing subtitles for: $file"
			filebot -get-subtitles "$file" --lang da --output srt --encoding utf8 -non-strict
			filebot -get-subtitles "$file" --lang en --output srt --encoding utf8 -non-strict
			echo ""

			sleep $SLEEP_DELAY

			download_subtitles "$file"
		fi
	done
}

movie_directory="$1"
download_subtitles "$movie_directory"


